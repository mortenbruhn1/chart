#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

chart_dir="charts/dependabot-gitlab"

update_dependencies

log_with_header "Validate chart template files"
for yaml in .gitlab/ci/kube/values/*.yaml; do
  info "Validating $chart_dir with $yaml" "-"

  helm template $chart_dir -f $yaml | kubeconform -strict -verbose -kubernetes-version "${KUBERNETES_VERSION}" \
    -schema-location default \
    -schema-location 'https://raw.githubusercontent.com/datreeio/CRDs-catalog/main/{{.Group}}/{{.ResourceKind}}_{{.ResourceAPIVersion}}.json'
done
